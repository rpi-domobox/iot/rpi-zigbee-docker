## Zigbee
#### Zigbee2MQTT

Official documentation : https://www.zigbee2mqtt.io/

Warning: You need to have a MQTT broker running with your own configuration  

**Without Docker Compose ** 
If you don't use docker-compose run the container with the following command
```sh
sudo docker run -it -v ./resources/zigbee/data:/app/data --device=/dev/ttyUSBZIGBEE koenkk/zigbee2mqtt:arm32v6
```

**With Docker Compose ** 
Run the command where you put the docker-compose.yml
```sh
sudo docker-compose up -d
```

You need to put your own MQTT broker configuration in the dedicated volume.
The configuration file (configuration.yaml) was created in the zigbee volume
```sh
$ cd ~/resources/zigbee/data
$ nano configuration.yaml
```

For securized broker
```sh
# Optional: MQTT server authentication user
user: {user}
# Optional: MQTT server authentication password
password: {password}
# Optional: MQTT client ID
client_id: '{client_id}'
```

For fixed ports
```sh
# Serial settings
serial:
  # Location of CC2531 USB sniffer
  port: /dev/ttyUSBZIGBEE
```

### Fix USB ports
https://www.domoticz.com/wiki/PersistentUSBDevices

Modify the com rules files
```sh
sudo nano /etc/udev/rules.d/99-usb-serial.rules
```
or
```sh
sudo nano /etc/udev/rules.d/99-com.rules

```
Add
```sh
SUBSYSTEM=="tty", ATTRS{idVendor}=="{ID_VENDOR_ID}", ATTRS{idProduct}=="{ID_MODEL_ID}", ATTRS{serial}=="{ID_SERIAL_SHORT}", SYMLINK+="ttyUSBZIGBEE"
```

For Zigbee USB-Stick 
```sh
$ sudo udevadm info --query=all --name=ttyACM0

P: /devices/platform/soc/3f980000.usb/usb1/1-1/1-1.1/1-1.1.3/1-1.1.3:1.0/tty/ttyACM0
N: ttyACM0
S: serial/by-id/usb-Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0-if00
S: serial/by-path/platform-3f980000.usb-usb-0:1.1.3:1.0
E: DEVLINKS=/dev/serial/by-path/platform-3f980000.usb-usb-0:1.1.3:1.0 /dev/serial/by-id/usb-Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0-if00
E: DEVNAME=/dev/ttyACM0
E: DEVPATH=/devices/platform/soc/3f980000.usb/usb1/1-1/1-1.1/1-1.1.3/1-1.1.3:1.0/tty/ttyACM0
E: ID_BUS=usb
E: ID_MODEL=TI_CC2531_USB_CDC
E: ID_MODEL_ENC=TI\x20CC2531\x20USB\x20CDC
E: ID_MODEL_ID=16a8
E: ID_PATH=platform-3f980000.usb-usb-0:1.1.3:1.0
E: ID_PATH_TAG=platform-3f980000_usb-usb-0_1_1_3_1_0
E: ID_REVISION=0009
E: ID_SERIAL=Texas_Instruments_TI_CC2531_USB_CDC___0X00124B0018E1CCD0
E: ID_SERIAL_SHORT=__0X00124B0018E1CCD0
E: ID_TYPE=generic
E: ID_USB_CLASS_FROM_DATABASE=Communications
E: ID_USB_DRIVER=cdc_acm
E: ID_USB_INTERFACES=:020201:0a0000:
E: ID_USB_INTERFACE_NUM=00
E: ID_VENDOR=Texas_Instruments
E: ID_VENDOR_ENC=Texas\x20Instruments
E: ID_VENDOR_FROM_DATABASE=Texas Instruments, Inc.
E: ID_VENDOR_ID=0451
E: MAJOR=166
E: MINOR=0
E: SUBSYSTEM=tty
E: TAGS=:systemd:
E: USEC_INITIALIZED=4879253
```